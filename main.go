package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"strings"
)

func main() {

	// Set up a CLI flag called "-config" to allow users
	// to supply the configuration file
	var port string
	flag.StringVar(&port, "p", ":8080", "port, begin with :")

	var boardSize int
	flag.IntVar(&boardSize, "b", 10, "Board size (>=1)")

	var receiverNumber int
	flag.IntVar(&receiverNumber, "n", 4, "Number of receivers deployed on the machine")

	var gran int
	flag.IntVar(&gran, "g", 0, "Granularity (number of steps in resolution made at each task)")

	var printSolution bool
	flag.BoolVar(&printSolution, "s", false, "Print solutions")

	var help bool
	flag.BoolVar(&help, "h", false, "Print this help")

	var isMaster bool
	flag.BoolVar(&isMaster, "m", false, "Is this node the master, if yes, receivers addr must be given")

	var addresses string
	flag.StringVar(&addresses, "a", "", "127.0.0.1:8081;127.0.0.1:8080 of the master and itself, separate by ;")

	// Actually parse the flags
	flag.Parse()

	if isMaster {
		ctx := launchNetwork(true)
		go masterStart(boardSize, gran, printSolution, ctx)
	} else {
		if len(addresses) == 0 {
			fmt.Println("IP addresses of the master (if receiver) or of the receivers (if master) must be given")
			flag.PrintDefaults()
			return
		}
		addressesSplit := strings.Split(addresses, ";")
		m := &foreignMaster{ip: addressesSplit[1]}
		ctx := launchNetwork(false)
		for i := 0; i < receiverNumber; i++ {
			rec := newReceiver(m, ctx)
			rec.start(addressesSplit[0])
		}
	}
	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatal(err)
	}

}
