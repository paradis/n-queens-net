package main

import (
	"log"
	"math"
	"time"
)

type Diagnostics struct {
	TotalComputeTimeMS int64 `json:"total_compute_time_ms"`
	TotalTaskProcessed int64 `json:"total_task_processed"`
	Rank               int   `json:"rank"`
	SolutionFound      int   `json:"solution_found"`
}

type receiver struct {
	rank int
	gran int
	m    *foreignMaster
	ctx  *commCtx
}

func newReceiver(m *foreignMaster, ctx *commCtx) *receiver {
	return &receiver{
		rank: -1,
		m:    m,
		ctx:  ctx,
	}
}

func (r *receiver) start(ipAddr string) {
	go func() {
		// I am available
		attachParams := r.m.sendAttach(r.ctx, ipAddr)
		r.rank = attachParams.Rank
		log.Printf("Receiver [%d] starting...", r.rank)
		log.Printf("Attached")

		diag := Diagnostics{
			TotalComputeTimeMS: 0,
			TotalTaskProcessed: 0,
			Rank:               r.rank,
		}

		for {
			r.m.sendState(r.rank, true)
			exitLoop := false
			select {
			case elt := <-r.m.receiveTask(r.ctx, r.rank): // Get an element to scan
				diag.TotalTaskProcessed++
				computeBeginTime := time.Now()
				var acc []*Board
				compute(elt.B, -1, elt.Row, elt.Rowmask, elt.Ldmask, elt.Rdmask, &acc)
				diag.TotalComputeTimeMS += time.Now().Sub(computeBeginTime).Milliseconds()
				var res []Board = make([]Board, len(acc))
				for i := 0; i < len(res); i++ {
					res[i] = *acc[i]
				}
				log.Println(len(res))
				if len(res) > 0 {
					diag.SolutionFound = diag.SolutionFound + len(res)
					r.m.sendResult(res)
				}
			case diagc := <-r.ctx.quit[r.rank]:
				diagc <- &diag
				exitLoop = true
				break // stop
			}
			if exitLoop {
				log.Printf("Stopped!!")
				break
			}
		}
	}()
}

func compute(b [][]bool, gran, row, rowmask, ldmask, rdmask int, acc *[]*Board) {
	n := len(b)
	all_rows_filled := (1 << n) - 1

	if gran == 0 || rowmask == all_rows_filled {
		brd := &Board{
			Finished: rowmask == all_rows_filled,
			B:        b,
			Row:      row,
			Rowmask:  rowmask,
			Ldmask:   ldmask,
			Rdmask:   rdmask,
		}
		*acc = append(*acc, brd)
		return
	}

	safe := all_rows_filled & (^(rowmask | ldmask | rdmask)) // all that are not attacked
	for safe != 0 {
		// Two's complement: if 127: 0111 1111 then -127: 1000 0001 => 0000 0001
		// Two's complement: if 126: 0111 1110 then -126: 1000 0010 => 0000 0010
		// Two's complement: if 124: 0111 1100 then -124: 1000 0100 => 0000 0100
		// This means take the next bit on the right at each step (linked to end of loop)
		p := safe & (-safe)
		col := int(math.Log2(float64(p))) // take the position of the bit in p
		b[row][col] = true
		// rowmask|p => set the bit "col" in rowmask to used
		// (ldmask|p)<<1 => indicate for the next row that the bit in diagonal on left is attacked
		// 00001000 = current
		// 00010000 = for next line
		// same for rdmask what gives in total:
		// 00001000 = current
		// 00010100 = for next line
		compute(b[:][:], gran-1, row+1, rowmask|p, (ldmask|p)<<1, (rdmask|p)>>1, acc)

		// if 127 :  0111 1111 then 126: 0111 1110 => 0111 1110 = 126
		// if 126 :  0111 1110 then 125: 0111 1101 => 0111 1100
		// This means, each time it is executed, it removes one bit on the right
		safe = safe & (safe - 1)

		b[row][col] = false
	}

}
