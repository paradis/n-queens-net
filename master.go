package main

import (
	"fmt"
	"log"
	"math"
	"time"
)

type statistics struct {
	totalComputationTimeMS int64
	totalTaskSent          int
	totalSolution          int
	rcvDiags               []*Diagnostics
	nbReceiver             int
	boardSize              int
	granularity            int
}

func masterStart(boardSize, granularity int, printSol bool, ctx *commCtx) {

	// create the statistics object
	stats := statistics{
		totalComputationTimeMS: 0,
		totalTaskSent:          0,
		totalSolution:          0,
		boardSize:              boardSize,
		granularity:            granularity,
	}
	log.Println("Master starting...")

	// create all the receivers
	var receivers []*foreignReceiver
	var readyFlags []bool
	// for i := 0; i < nbReceiver; i++ {
	// 	receivers[i] = &foreignReceiver{
	// 		rank: i,
	// 		ip:   receiversAddr[i],
	// 	}
	// }

	// Compute the first boards
	var initialBoards []*Board
	initialBoard := newBoard(boardSize)
	compute(
		initialBoard.B, granularity, initialBoard.Row, initialBoard.Rowmask, initialBoard.Ldmask, initialBoard.Rdmask,
		&initialBoards,
	)

	var results []Board
	log.Println(len(initialBoards))

	timeSet := false
	var beginTime time.Time
	var endTime time.Time
	for {
		// distribute the tasks
		for i, r := range receivers {
			if readyFlags[i] && len(initialBoards) > 0 {
				if !timeSet {
					beginTime = time.Now()
					timeSet = true
				}
				// send task to this receiver
				r.sendTask(*initialBoards[0], 10)
				initialBoards = initialBoards[1:]
				readyFlags[i] = false
				stats.totalTaskSent++
			}
		}

		// Check if a new results come from receivers
		select {
		case elt := <-receiveResult(ctx): // Get an element to scan
			stats.totalSolution += len(elt)
			if printSol {
				for _, board := range elt {
					printBoard(&board)
				}
			}
			results = append(results, elt...)
		default:
		}

		// Check if a new receiver wants to attach itself
		select {
		case rcv := <-receiveNewReceivers(ctx): // Get an element to scan
			log.Printf("Receiver [%d] is attached\n", rcv.rank)
			receivers = append(receivers, rcv)
			readyFlags = append(readyFlags, false)
			stats.rcvDiags = append(stats.rcvDiags, nil)
			stats.nbReceiver++
		default:
		}

		// check if receiver is ready
		for i, r := range receivers {
			select {
			case isReady := <-r.receiveState(ctx):
				readyFlags[i] = isReady
			default:
			}
		}

		// check if termination
		allReady := true
		for _, flag := range readyFlags {
			allReady = allReady && flag
		}
		if allReady && len(initialBoards) == 0 {
			endTime = time.Now()
			stats.totalComputationTimeMS = endTime.Sub(beginTime).Milliseconds()
			log.Println("Termination detected")
			for i, r := range receivers {
				stats.rcvDiags[i] = r.sendStop()
			}
			break
		}
	}
	printStats(&stats)
}

func printBoard(b *Board) {
	fmt.Println("\n\nSolution :")
	for i := 0; i < len(b.B); i++ {
		for j := 0; j < len(b.B); j++ {
			toWrite := "O"
			if b.B[i][j] {
				toWrite = "X"
			}
			fmt.Print(toWrite)
		}
		fmt.Println()
	}
}

func printStats(stats *statistics) {
	totalSol := 0
	for _, s := range stats.rcvDiags {
		totalSol += s.SolutionFound
	}
	fmt.Println("----------------------")
	fmt.Println("      STATISTICS      ")
	fmt.Println("----------------------")
	fmt.Println("Global values:        ")
	fmt.Printf("      Board size:      %d\n", stats.boardSize)
	fmt.Printf("      Nb Proc:         %d\n", stats.nbReceiver)
	fmt.Printf("      Granularity:     %d\n", stats.granularity)
	fmt.Printf("      Solutions  :     %d\n", totalSol)
	fmt.Println("Processing metrics:        ")
	fmt.Printf("      Total time[ms]:  %d\n", stats.totalComputationTimeMS)
	fmt.Printf("      Total tasks:     %d\n", stats.totalTaskSent)
	fmt.Println("Receivers metrics:        ")
	var rcvTotalComputeTime int64 = 0
	for _, diag := range stats.rcvDiags {
		rcvTotalComputeTime += diag.TotalComputeTimeMS
		fmt.Printf(
			"      Rank [%d] : tasks : %d / time[ms]: %d\n", diag.Rank, diag.TotalTaskProcessed,
			diag.TotalComputeTimeMS,
		)
	}
	fmt.Println("Overall metrics:        ")
	if stats.totalComputationTimeMS > 0 {
		comp := rcvTotalComputeTime * 100 / stats.totalComputationTimeMS
		transf := 100 - comp
		fmt.Printf("      Time repartition: %d %% computation vs %d %% transfert\n", comp, transf)
	}
	loadBalancingGoal := float64(stats.totalTaskSent / stats.nbReceiver)
	totalDiffGoal := 0.0
	for _, diag := range stats.rcvDiags {
		totalDiffGoal += math.Abs(loadBalancingGoal - float64(diag.TotalTaskProcessed))
	}
	fmt.Printf(
		"      Load balancing effectiveness: %f  \n",
		totalDiffGoal/float64(stats.nbReceiver)/loadBalancingGoal,
	)
	// tricky...
	log.Fatalf("Program finished")
}
