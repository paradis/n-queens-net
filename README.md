## Best result:
```text
----------------------
      STATISTICS      
----------------------
Global values:        
      Board size:      17
      Nb Proc:         4
      Granularity:     9
      Solutions  :     95815104
Processing metrics:        
      Total time[ms]:  603595
      Total tasks:     127045453
Receivers metrics:        
      Rank [0] : tasks : 1 / time[ms]: 603594
      Rank [1] : tasks : 74942644 / time[ms]: 2407
      Rank [2] : tasks : 30305873 / time[ms]: 2067
      Rank [3] : tasks : 21796935 / time[ms]: 1631
Overall metrics:        
      Time repartition: 101 % computation vs -1 % transfert
      Load balancing effectiveness: 0.679777  
```

Evolution for 12 boards regarding the grand:

| Granularity | Execution time [ms] | Number of task created|
|-------------|------------------------|---------------|
| 1           | 1992                   |   841989      |
| 2 | 1016 | 413281 |
| 3 | 720 | 276333 |
| 4 | 556 | 199351 |
| 5 | 456 | 177817 |
| 6 | 183 | 52857 |
| 7 | 387 | 120105 |
| 5 | 610 | 195271 |
| 9 | 676 | 222721 |
| 10 | 474 | 160965 |
| 11 | 296 | 68265 |
| 12 | 160 | 1 |


Pour 14:

| Granularity | Execution time [ms] | Number of task created|
|-------------|------------------------|---------------|
| 1           | 64653                   |   26992957      |
| 2 | 33011 | 13298095 |
| 3 | 25294 | 9146759 |
| 4 | 19497 | 6733423 |
| 5 | 18539 | 6377101 |
| 6 | 13748 | 4753407 |
| 7 | 3499 | 835057 |
| 5 | 8003 | 2211869 |
| 9 | 15712 | 4391989 |
| 10 | 22563 | 6323033 |
| 11 | 22192 | 6471873 |
| 12 | 15128 | 4511923 |
| 13 | 6677 | 1940501 |
| 13 | 4160 | 1 |

