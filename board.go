package main

type Board struct {
	Finished bool     `json:"finished"`
	B        [][]bool `json:"b"`
	Row     int `json:"row"`
	Rowmask int `json:"rowmask"`
	Ldmask int `json:"ldmask"`
	Rdmask int `json:"rdmask"`
}

func newBoard(size int) *Board {
	// create grid
	grid := make([][]bool, size)
	for i := range grid {
		grid[i] = make([]bool, size)
	}
	return &Board{
		Finished: false,
		B:        grid,
		Row:      0,
		Rowmask:  0,
		Ldmask:   0,
		Rdmask:   0,
	}
}