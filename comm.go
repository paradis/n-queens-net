package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

type foreignReceiver struct {
	rank int
	ip   string
}

type foreignMaster struct {
	ip string
}

var client = http.Client{
	Timeout: 30 * time.Second,
}

type commCtx struct {
	taskRcv      map[int]chan Board // channel for the receiver to receive task
	resultsChan  chan []Board       // channel for the receiver to send task
	state        []chan bool        // channel for the receiver to send the task
	quit         map[int]chan chan *Diagnostics
	newReceivers chan *foreignReceiver
	isMaster     bool
}

type Task struct {
	B    Board `json:"b"`
	Rank int   `json:"rank"`
}

type AttachRequest struct {
	IPAddr string `json:"ip"`
}

type AttachResponse struct {
	Rank int `json:"rank"`
}

type State struct {
	IsReady bool `json:"isReady"`
	Rank    int  `json:"rank"`
}

type Stop struct {
	Rank int `json:"rank"`
}

type handler struct {
	ctx *commCtx
}

var rankConter int = 0
var cntMutex sync.Mutex

func launchNetwork(isMaster bool) *commCtx {
	var ctx commCtx
	if isMaster {
		ctx = commCtx{
			taskRcv:      nil,
			resultsChan:  make(chan []Board, 100000),
			state:        make([]chan bool, 0),
			newReceivers: make(chan *foreignReceiver, 1000),
			isMaster:     true,
		}
		h := handler{ctx: &ctx}
		http.HandleFunc("/result", h.httpRcvResult)
		http.HandleFunc("/state", h.httpRcvState)
		http.HandleFunc("/attach", h.httpRcvAttach)
	} else {
		ctx = commCtx{
			taskRcv:     make(map[int]chan Board, 0),
			resultsChan: nil,
			state:       nil,
			isMaster:    false,
			quit:        make(map[int]chan chan *Diagnostics, 0),
		}
		h := handler{ctx: &ctx}
		http.HandleFunc("/task", h.httpRcvTask)
		http.HandleFunc("/stop", h.httpRcvStop)
	}
	return &ctx
}

func (h *handler) httpRcvResult(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var res []Board
	err := decoder.Decode(&res)
	if err != nil {
		log.Fatalf("error when trying to unmarshall the results: %s", err.Error())
	}
	h.ctx.resultsChan <- res
	defer r.Body.Close()
	r.Close = true
}

func (h *handler) httpRcvState(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var s State
	err := decoder.Decode(&s)
	if err != nil {
		log.Fatalf("error when trying to unmarshall the state: %s", err.Error())
	}
	h.ctx.state[s.Rank] <- s.IsReady
	r.Close = true
	defer r.Body.Close()
}

func (h *handler) httpRcvTask(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var res Task
	err := decoder.Decode(&res)
	if err != nil {
		log.Fatalf("error when trying to unmarshall the results: %s", err.Error())
	}
	h.ctx.taskRcv[res.Rank] <- res.B
	defer r.Body.Close()
	r.Close = true
}

func (h *handler) httpRcvAttach(w http.ResponseWriter, r *http.Request) {
	log.Println("HTTP Attach Received")
	decoder := json.NewDecoder(r.Body)
	var res AttachRequest
	err := decoder.Decode(&res)
	if err != nil {
		log.Fatalf("error when trying to unmarshall the results: %s", err.Error())
	}

	cntMutex.Lock()
	newRank := rankConter
	rankConter++
	cntMutex.Unlock()
	rcv := &foreignReceiver{
		rank: newRank,
		ip:   res.IPAddr,
	}

	h.ctx.state = append(h.ctx.state, make(chan bool, 0))
	h.ctx.newReceivers <- rcv

	retData := &AttachResponse{
		Rank: newRank,
	}
	body, _ := json.Marshal(retData)
	w.Write(body)

	defer r.Body.Close()
	r.Close = true
}

func (h *handler) httpRcvStop(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var res Stop
	err := decoder.Decode(&res)
	if err != nil {
		log.Fatalf("error when trying to unmarshall the results: %s", err.Error())
	}

	diagc := make(chan *Diagnostics)
	defer close(diagc)
	h.ctx.quit[res.Rank] <- diagc
	body, _ := json.Marshal(<-diagc)
	_, err = w.Write(body)
	if err != nil {
		log.Fatalf("error when trying to send diagnostics: %s", err.Error())
	}
	r.Body.Close()
	r.Close = true
	return
}

func (r *foreignReceiver) receiveState(ctx *commCtx) chan bool {
	return ctx.state[r.rank]
}

func (m *foreignMaster) receiveTask(ctx *commCtx, rank int) chan Board {
	return ctx.taskRcv[rank]
}

func receiveResult(ctx *commCtx) <-chan []Board {
	return ctx.resultsChan
}

func receiveNewReceivers(ctx *commCtx) <-chan *foreignReceiver {
	return ctx.newReceivers
}

func (r *foreignReceiver) sendTask(b Board, gran int) {
	task := &Task{
		B:    b,
		Rank: r.rank,
	}
	body, _ := json.Marshal(task)
	resp, err := client.Post("http://"+r.ip+"/task", "application/json", bytes.NewBuffer(body))
	if err != nil {
		return
	}
	defer resp.Body.Close()
}

func (m *foreignMaster) sendAttach(ctx *commCtx, ipAddr string) *AttachResponse {
	attach := &AttachRequest{IPAddr: ipAddr}
	body, _ := json.Marshal(attach)

	resp, err := client.Post("http://"+m.ip+"/attach", "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Printf(err.Error())
	}

	decoder := json.NewDecoder(resp.Body)
	var res AttachResponse
	err = decoder.Decode(&res)
	if err != nil {
		return nil
	}
	// create all channels for this rank:
	ctx.taskRcv[res.Rank] = make(chan Board, 100)
	ctx.quit[res.Rank] = make(chan chan *Diagnostics, 1)
	resp.Body.Close()
	log.Printf("Attached [%d]", res.Rank)
	return &res
}

func (m *foreignMaster) sendState(rank int, state bool) {
	s := &State{
		IsReady: state,
		Rank:    rank,
	}
	body, _ := json.Marshal(s)
	resp, err := client.Post("http://"+m.ip+"/state", "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Printf(err.Error())
		return
	}
	resp.Close = true
	defer resp.Body.Close()
}

func (m *foreignMaster) sendResult(bs []Board) bool {
	body, err := json.Marshal(bs)
	if err != nil {
		return false
	}
	resp, err := client.Post("http://"+m.ip+"/result", "application/json", bytes.NewBuffer(body))
	if err != nil {
		return false
	}

	defer resp.Body.Close()
	//Read the response body
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	} else {
		a := string(respBody)
		return len(a) == len(a)
	}
	return true

}

func (r *foreignReceiver) sendStop() *Diagnostics {
	s := &Stop{Rank: r.rank}
	body, err := json.Marshal(s)
	if err != nil {
		log.Printf(err.Error())
	}
	resp, err := client.Post("http://"+r.ip+"/stop", "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Printf(err.Error())
	}
	decoder := json.NewDecoder(resp.Body)
	var res Diagnostics
	err = decoder.Decode(&res)
	if err != nil {
		log.Printf("Error when receiving diagnostics %s", err.Error())
	}
	resp.Body.Close()
	return &res
}
